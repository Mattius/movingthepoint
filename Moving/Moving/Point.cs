﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Moving
{
    class Punto
    {
        int x;
        int y;
        public Punto(int xInicial, int yInicial)
        {
            x = xInicial;
            y = yInicial;
        }
        public void MoverArriba()
        {
            y = y - 1;
        }
        public void MoverAbajo()
        {
            y = y + 1;
        }
        public void MoverDerecha()
        {
            x = x + 1;
        }
        public void MoverIzquierda()
        {
            x = x - 1;
        }
        public void Mostrar()
        {
            Console.SetCursorPosition(x, y);
            Console.ForegroundColor = ConsoleColor.Gray;
            Console.Write("+");
        }
        public void mostrarA()
        {
            Console.SetCursorPosition(x, y);
            Console.ForegroundColor = ConsoleColor.Blue;
            Console.Write("+");
        }
    }
}
