﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Moving
{
    class Program
    {
        static void Main()
        {
            int xIncial, yInicial, xFinal, yFinal;
            bool correcto;

            do
            {
                Console.Write("Punto 1:Ingrese un valor entre 0-70 para x:");
                correcto = int.TryParse(Console.ReadLine(), out xIncial);


                if (!correcto || xIncial > 70 || xIncial < 0)
                {

                    Console.Write("Por favor digite un número dentro del rango admitido");
                    Console.ReadKey();
                    Console.Clear();


                }


            }
            while (!correcto || xIncial > 70 || xIncial < 0);
            Console.Write("El número ingresado es correcto: x={0}", xIncial);
            Console.ReadKey();

            do
            {
                Console.Write("Punto 1:Ingrese un valor entre 0-20 para y:");
                correcto = int.TryParse(Console.ReadLine(), out yInicial);
                if (!correcto || yInicial > 20 || yInicial < 0)
                {

                    Console.Write("Por favor digite un número dentro del rango admitido");
                    Console.ReadKey();
                    Console.Clear();


                }


            }
            while (!correcto || yInicial > 20 || yInicial < 0);
            Console.Write("El número ingresado es correcto: y={0}", yInicial);
            Console.ReadKey();

            Punto p1 = new Punto(xIncial, yInicial);

            /*Esta parte del código la voy a modificar cambiando todas las xInicial por xFinal y todas las yInicial por yFinal*/

            do
            {
                Console.Write("Punto 2:Ingrese un valor entre 0-70 para x:");
                correcto = int.TryParse(Console.ReadLine(), out xFinal);


                if (!correcto || xFinal > 70 || xFinal < 0)
                {

                    Console.Write("Por favor digite un número dentro del rango admitido");
                    Console.ReadKey();
                    Console.Clear();


                }


            }
            while (!correcto || xFinal > 70 || xFinal < 0);
            Console.Write("El número ingresado es correcto: x={0}", xFinal);
            Console.ReadKey();

            do
            {
                Console.Write("Punto 2:Ingrese un valor entre 0-20 para y:");
                correcto = int.TryParse(Console.ReadLine(), out yFinal);
                if (!correcto || yFinal > 20 || yFinal < 0)
                {

                    Console.Write("Por favor digite un número dentro del rango admitido");
                    Console.ReadKey();
                    Console.Clear();


                }


            }
            while (!correcto || yFinal > 20 || yFinal < 0);
            Console.Write("El número ingresado es correcto: y={0}", yFinal);
            Console.ReadKey();
            Console.Clear();

            Punto p2 = new Punto(xFinal, yFinal);
            Console.Clear();
            p1.Mostrar();
            p2.Mostrar();

            if (xIncial < xFinal)
            {
                while (xIncial < xFinal)
                {
                    p1.MoverDerecha();
                    p1.mostrarA();
                    xIncial++;
                }
            }
            else
            {
                while (xIncial > xFinal)
                {
                    p1.MoverIzquierda();
                    p1.mostrarA();
                    xIncial--;
                }
            }
            if (yInicial < yFinal)
            {
                while (yInicial + 1 < yFinal)
                {
                    p1.MoverAbajo();
                    p1.mostrarA();
                    yInicial++;
                }
            }
            else
            {
                while (yInicial - 1 > yFinal)
                {
                    p1.MoverArriba();
                    p1.mostrarA();
                    yInicial--;
                }
            }
            Console.ReadKey();
        }
    }
}
